#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//#include <string.h>
//#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/inotify.h>
#include "functions/common.h"
#include "functions/file_operations.h"
#include "functions/socket_functions.h"
#include <values.h>
#include <signal.h>
#include <wait.h>

#define NOTIFY_BUF_LEN (10 * (sizeof(struct inotify_event) + NAME_MAX+ 1))

void execute_server_response(qbox_container_t qbc, int socketfd);
qbox_container_t sync_with_server(struct hostent *server, int portno);
void parse_client_args(int argc, char **argv,char * dir_name, struct hostent **serv_hostent, int *port_nr) ;

void touch_db_file(int signo);
int main(int argc, char *argv[]) {
    int portno;
    struct hostent *server;
    char dir_name[BUF_BIG];
    parse_client_args(argc, argv,dir_name, &server, &portno);

    chdir(dir_name);

    sync_with_server(server, portno);

    int inotify_fd, watch;



    char buf[NOTIFY_BUF_LEN];
    int my_mode = IN_MOVE | IN_MODIFY |  IN_DELETE | IN_CLOSE_WRITE;


    ssize_t numRead;
    int update_count=0;
    int forever=1;

    /// get updated qbc
    qbox_container_t updated_qbc;

    //In order to trigger inotify and sync even if user hasn't changed anything.
    qbc_load_form_file(&updated_qbc,CLIENT_DB_NAME);
    signal(SIGALRM,touch_db_file);

    while (forever) {
        sleep(2);
        /// 1. Load setups watches on folders from qbc_updated
        // 2. Wait for notify
        // 3. close inotify
        // 4 sync with serv
/*
        if((inotify_fd = inotify_init())<0)printAndQuit("Cannot init inotify\n");

        ///monitoring:

        //main dir
        if(inotify_add_watch(inotify_fd, ".", my_mode)<0)
            printAndQuit("Cannot monitor main dir.");
        //and subdirs:
        for(int j=0;j<updated_qbc.i;j++){
            if (updated_qbc.arr[j].file_type== FILETYPE_DIR){
                inotify_add_watch(inotify_fd,updated_qbc.arr[j].filename,my_mode);
            }
        }

*/
        free(updated_qbc.arr);// Prevent memory leakage :)

        printf("Loop %d\n",update_count++);
  /*      alarm(MAX_SYNC_INTERVAL);
        numRead = read(inotify_fd, buf, NOTIFY_BUF_LEN);

        if (numRead == 0) {
            printAndQuit("read() from inotify fd returned 0!");
        }
        if (numRead < 0) {
            printAndQuit("Inotify read error");
        }
        usleep(5000);
        close(inotify_fd);
*/
        updated_qbc = sync_with_server(server, portno);


    }
}

void touch_db_file(int signo){
    pid_t pid=fork();
    if (pid==0){
        execlp("touch","touch",CLIENT_DB_NAME,0);
    } else{
        waitpid(pid,NULL,0);
        printf("!!!!!!!!!!!Pid taken care of\n");
    }
}

qbox_container_t sync_with_server(struct hostent *server, int portno) {

    qbox_container_t serv_response, qbc_old, qbc_new, qbc_diff, updated_qbc;
    int sockfd;
    struct sockaddr_in serv_addr;

    //setup socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        fprintf(stderr, "Cannot setup socket \n");
        exit(1);
    }

    setup_serv_addr(&serv_addr, server, portno);


    qbc_new = qbc_form_tree();
    qbc_load_form_file(&qbc_old, CLIENT_DB_NAME);
    qbc_diff = qbc_diff_client(qbc_new, qbc_old);
//    qbc_pretty_print(qbc_diff, 1);

    if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        shutdown(sockfd, SHUT_RDWR);
        close(sockfd);
        fprintf(stderr, "Cannot connect to server \n");
        exit(1);
    }
    send_int_through_socket(CON_CLI, sockfd);

    send_qbc_to_socket(qbc_diff, sockfd);


    save_qbc_from_socket(&serv_response, sockfd);

    if (serv_response.i != 0) {
//        printf("serv response\n");
//        qbc_print(serv_response);
        execute_server_response(serv_response, sockfd);
    }


    updated_qbc = qbc_empty((size_t) qbc_diff.maxI);
    for (int i = 0; i < qbc_diff.i; i++)
        if (qbc_diff.arr[i].action != ACTION_DELETE)
            qbc_append_with_action(&updated_qbc, qbc_diff.arr[i], NO_ACTION);
    qbc_save_to_file(updated_qbc, CLIENT_DB_NAME);

    free(qbc_diff.arr);
    free(qbc_new.arr);
    free(qbc_old.arr);
//    free(updated_qbc.arr);
    if (serv_response.i != 0)
        free(serv_response.arr);


    shutdown(sockfd, SHUT_RDWR);
    close(sockfd);
    return updated_qbc;
}



/**
 *
 * @param qbc - response for server
 * @param socketfd - id of open socked used to communicate with server
 */
void execute_server_response(qbox_container_t qbc, int socketfd) {
    qbox_db_record_t *qbr;

    qbox_container_t dirs_to_rm = qbc_empty(20);
    qbox_container_t files_to_rm = qbc_empty(20);



    for (int i = 0; i < qbc.i; i++) {
        qbr = &qbc.arr[i];
        switch (qbr->action) {
            case ACTION_ADD:
                if (qbr->file_type == FILETYPE_DIR)
                    mkdir(qbr->filename, DEFAULT_MODE);
                else {
                    if (save_file_from_socket(socketfd) < 0)
                        fprintf(stderr, "cannot save file");
                    printf("File saved %s\n", qbr->filename);
                }
                break;
            case ACTION_DELETE:
                if (qbr->file_type == FILETYPE_DIR)
                    qbc_append(&dirs_to_rm, *qbr);
                else {
                    qbc_append(&files_to_rm, *qbr);
                }
//                if (unlink(qbr->filename) < 0)
//                    fprintf(stderr, "cannot delete file");
//                printf("File deleted %s\n", qbr->filename);
                break;
            case ACTION_UPDATE:
                if (unlink(qbr->filename) < 0 && rmdir(qbr->filename) < 0)
                    fprintf(stderr, "cannot delete file/rm dir");

                if (qbr->file_type == FILETYPE_DIR) {
                    mkdir(qbr->filename, DEFAULT_MODE);
                    printf("Dir created %s \n", qbr->filename);
                } else {
                    if (save_file_from_socket(socketfd) < 0)
                        fprintf(stderr, "cannot save file");
                    printf("File updated%s\n", qbr->filename);
                }
                break;

            case ACTION_REQUEST:
                if (qbr->file_type == FILETYPE_DIR)
                    fprintf(stderr, "Request for dir in not intended to be in use, check code\n");
                if (send_file_to_socket(qbr->filename, socketfd) < 0)
                    printf("Cannot send file \n");
                else
                    printf("Sent file %s\n", qbr->filename);
                break;

            default:
                printAndQuit("WRONG ACTION  while processing server response %d\n", qbr->action);

        }
    }
    for (int i = 0; i < files_to_rm.i; i++) {
        unlink(files_to_rm.arr[i].filename);
        printf("File %s removed \n", dirs_to_rm.arr[i].filename);
    }

    for (int i = 0; i < dirs_to_rm.i; i++) {
        rmdir(dirs_to_rm.arr[i].filename);
        printf("Dir %s removed \n", dirs_to_rm.arr[i].filename);
    }


}
void parse_client_args(int argc, char **argv,char * dir_name, struct hostent **serv_hostent, int *port_nr) {


    if (argc < 4) {
        fprintf(stderr, "usage %s dir_name hostname port\n", argv[0]);
        exit(0);
    }

    strcpy(dir_name,argv[1]);
    if(access(dir_name,0)!=0)
        printAndQuit("Dir %s doesn't exist.\n",dir_name);

    *serv_hostent = gethostbyname(argv[2]);
    if (*serv_hostent == NULL) {
        fprintf(stderr, "ERROR, no such host\n");
        exit(0);
    }
    *port_nr = atoi(argv[3]);
}
