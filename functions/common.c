
#include "common.h"

void printAndQuit(const char *str, ...) {
    va_list args;
    va_start (args, str);
    vfprintf(stderr, str, args);
    va_end (args);

    fprintf(stderr, "\n%s\n", strerror(errno));
    exit(1);
}


int name_ignored(char *filename) {
    return (strcmp(filename, SERVER_DB_NAME) == 0 || strcmp(filename, ARCHIVE_DIR_NAME) == 0 ||
            strcmp(filename, CLIENT_DB_NAME) == 0 || strcmp(filename, ".") == 0 || strcmp(filename, "..") == 0);
}

struct qbox_container_t qbc_empty(size_t size) {
    qbox_container_t res;
    res.i = 0;
    res.maxI = (int) size;
    res.arr = calloc(size, sizeof(qbox_db_record_t));
    return res;
}

/**
 *
 * @param action
 * @param type
 * @param fname
 * @param sec
 * @param n_sec
 * @return
 */
qbox_db_record_t qbr_create(short action, file_type_t type, char *fname, time_t sec, long n_sec) {
    qbox_db_record_t res;
    strcpy(res.filename, fname);
    res.action = action;
    res.file_type = type;
    res.last_modification.tv_sec = sec;
    res.last_modification.tv_nsec = n_sec;// not important,
    return res;
}


void qbc_append_with_action(qbox_container_t *qbc, qbox_db_record_t qbr, short action_override) {
    qbr.action = action_override;
    qbc_append(qbc, qbr);


}

void qbc_append(qbox_container_t *qbc, qbox_db_record_t qbr) {
    if (qbc->i == qbc->maxI) {
        qbc->maxI += 100;
        qbc->arr = realloc(qbc->arr, (size_t) qbc->maxI * sizeof(qbox_db_record_t));
    }
    qbc->arr[qbc->i++] = qbr;

}


int comp_qbr_names(qbox_db_record_t const *qbr1, qbox_db_record_t const *qbr2) {
//        qbr1<qbr2 -> -1
//        qbr1==qbr2 -> 0
//        qbr1 > qbr2 -> 1
    return strcmp(qbr1->filename, qbr2->filename);

}

int comp_qbr_time(qbox_db_record_t const *qbr1, qbox_db_record_t const *qbr2) {
    if (qbr1->last_modification.tv_sec < qbr2->last_modification.tv_sec)return -1;
    if (qbr1->last_modification.tv_sec > qbr2->last_modification.tv_sec)return 1;

//    qbr1->last_modification.tv_sec==qbr2->last_modification.tv_sec, so:
    if (qbr1->last_modification.tv_nsec < qbr2->last_modification.tv_nsec)return -1;
    if (qbr1->last_modification.tv_nsec > qbr2->last_modification.tv_nsec)return 1;
    return 0;
}

void sort_qbc_by_name(qbox_container_t *qbc) {
    //ISO forbids nested functions, but it makes code way less messy
    int comp(const void *elem1, const void *elem2) {
        const qbox_db_record_t *qbr1, *qbr2;
        qbr1 = elem1;
        qbr2 = elem2;
//        return comp_qbr_names(qbr1,qbr2);
//       below - faster than above.
        return strcmp(qbr1->filename, qbr2->filename);

    }
    qsort(qbc->arr, (size_t) qbc->i, sizeof(qbox_db_record_t), comp);
}


qbox_container_t qbc_form_tree() {
    qbox_container_t result = qbc_empty(100);

    //ISO forbids nested functions, but it makes code way less messy
    int save_tree_to_result(const char *path, const struct stat *stats, int type, struct FTW *ftw) {
        char *filename;
        filename = basename((char *) path);
        if (name_ignored(filename))
            return 0;

        qbox_db_record_t qbr;


        //TODO - support links?
        if (S_ISDIR(stats->st_mode))
            qbr.file_type = FILETYPE_DIR;
        else if (S_ISREG(stats->st_mode))
            qbr.file_type = FILETYPE_REGULAR;
        else
            fprintf(stderr, "Not reg, not dir, links not supported\n");


        strcpy(qbr.filename, path);
        qbr.action = NO_ACTION;
        qbr.last_modification.tv_nsec = stats->st_mtim.tv_nsec;
        qbr.last_modification.tv_sec = stats->st_mtim.tv_sec;

        qbc_append(&result, qbr);

        return 0;
    }


    nftw(".", save_tree_to_result, 32, FTW_CHDIR);
    sort_qbc_by_name(&result);

    return result;
}

/**
 *\ Fuction mainly for debugging because it prints not parsed timespec
 * \Efect
 * prints qbox_record_t as  "ACT | t | fname | sec | nsec"
 */
void qbr_print(qbox_db_record_t qbr) {
    static char type[4];

    switch (qbr.action) {
        case ACTION_ADD:
            strcpy(type, "ADD");
            break;
        case ACTION_DELETE:
            strcpy(type, "DEL");
            break;
        case ACTION_UPDATE:
            strcpy(type, "UPD");
            break;
        case NO_ACTION:
            strcpy(type, "NOP");
            break;
        case ACTION_REQUEST:
            strcpy(type, "NOP");
            break;
        default:
            printAndQuit("WRONG ACTION %d\n", qbr.action);

    }
    printf("%s| %c | %s | %li | %li\n", type, (qbr.file_type == FILETYPE_REGULAR ? 'r' : 'd'), qbr.filename,
           qbr.last_modification.tv_sec, qbr.last_modification.tv_nsec);
}

/**
 * \brief Prints qbox_container_t using qbr_print
 */
void qbc_print(qbox_container_t qbc) {
    for (int i = 0; i < qbc.i; ++i) {
        printf("%d:: ", i);
        qbr_print(qbc.arr[i]);
    }

}


/**
 * @brief Prints qbox_container_t in readable manner using qbr_pretty_print
 * @param qbc - qbox_container_t to print
 * @param print_action : 0 - not print action, 1 - print action. Other - undefined action'
 * 
 */
void qbc_pretty_print(qbox_container_t qbc, int print_action) {
    for (int i = 0; i < qbc.i; ++i) {
        printf("%d ::", i);
        qbr_pretty_print(qbc.arr[i], print_action);
    }
}

/**
 * @brief prints qbox_record_t in human readable manner
 * @param qbr - qbox_record_t to print
 * @param print_action : 0 - not print action, 1 - print action. Other - undefined action'
 *
 *\Format
 * [ACT | ] file_type | file_name | time_of_last_modification
 */

void qbr_pretty_print(qbox_db_record_t qbr, int print_action) {
//    size_t str_size = sizeof("Wed Jun 30 21:49:08 1993\n") + 1;
    char *ct_str = ctime(&qbr.last_modification.tv_sec);

    if (print_action) {
        char action_str[4];
        switch (qbr.action) {
            case ACTION_ADD:
                strcpy(action_str, "ADD");
                break;
            case ACTION_DELETE:
                strcpy(action_str, "DEL");
                break;
            case ACTION_UPDATE:
                strcpy(action_str, "UPD");
                break;
            case NO_ACTION:
                strcpy(action_str, "NOP");
                break;
            case ACTION_REQUEST:
                strcpy(action_str, "REQ");
                break;
            default:
                printAndQuit("WRONG ACTION %d\n", qbr.action);

        }

        printf("%s | %c | %s | %s", action_str, (qbr.file_type == FILETYPE_REGULAR ? 'r' : 'd'), qbr.filename, ct_str);
    } else {
        printf(" %c | %s | %s", (qbr.file_type == FILETYPE_REGULAR ? 'r' : 'd'), qbr.filename, ct_str);
    }

}


/**
 * Saves qbc to created file. If file exists it will be truncated.
 * @param qbc - qbox_container_t to save
 * @param filename
 * @return 0 on success, negative number on failure.
 */
int qbc_save_to_file(qbox_container_t qbc, const char *filename) {
    int fd = open(filename, O_CREAT | O_WRONLY | O_TRUNC, DEFAULT_MODE);
    if (fd < 0)
        return -1;

    if (write(fd, qbc.arr, sizeof(qbox_db_record_t) * qbc.i) < sizeof(qbox_db_record_t) * qbc.i)
        return -1;

    close(fd);
    return 0;
}

/**
 *load qbc from file to which it was proably saved using qbc_save_to_file
 *
 * @param qbc - pointer to allocated qbox_container_t its values will be set ( including arr which will be allocated dynamically)
 * @param filename
 * @return 1 if there is no file and qbc_empty is executed, 0 on successful loading, -1 on failure
 */
int qbc_load_form_file(qbox_container_t *qbc, const char *filename) {

    int fd = open(filename, O_RDONLY);

    if (fd < 0) {
        //todo - remove print
        fprintf(stderr, "Couldn't find qbc file. Assuming empty\n");
        *qbc = qbc_empty(20);
        return 1;
    }
    struct stat st;
    fstat(fd, &st);

    qbc->arr = malloc((size_t) st.st_size);
    if (qbc->arr == NULL || read(fd, qbc->arr, (size_t) st.st_size) < 0) {
        fprintf(stderr, "Error in qbc_load_from_file\n");
        *qbc = qbc_empty(20);
        return -1;
    }
    close(fd);
    qbc->maxI = qbc->i = (int) st.st_size / sizeof(qbox_db_record_t);


    sort_qbc_by_name(qbc);   //TODO - rm? needed?
    return 0;
}

/**
 *
 * @param new_qbc
 * @param old_qbc
 * @return  qbox_container_t  -  Sum (set sum) of new_qbc and old qbc, but qbox_record_t's action will be changed in following manner:
*
 * if qbr is in new,and old with the same timestamp  action will be NO_ACTION
 *
 * if qbr is in new, but not in old :  ACTION_ADD
 *
 * if qbr is in old, but not in new: ACTION_DELETE
 *
 * if qbr is in new and old and in new timestamp is updated - action will be ACTION_UPDATE
 */
qbox_container_t qbc_diff_client(qbox_container_t new_qbc, qbox_container_t old_qbc) {

    int i_old = 0, i_new = 0;
    qbox_container_t res_qbc = qbc_empty((size_t) old_qbc.maxI);

    int comp;
    qbox_db_record_t qbr_tmp;
    while (i_old < old_qbc.i && i_new < new_qbc.i) {

        comp = comp_qbr_names(&(old_qbc.arr[i_old]), &(new_qbc.arr[i_new]));

        //old's name <new's name -- delete old
        // Old    New
        // a      -         i_old
        // b      b             i_new
        if (comp < 0) {

            qbr_tmp = old_qbc.arr[i_old];
            qbr_tmp.action = ACTION_DELETE;
            time(&qbr_tmp.last_modification.tv_sec);
            i_old++;

        } else if (comp == 0) {// new  name = old name
            qbr_tmp = new_qbc.arr[i_new];
            if (comp_qbr_time(&(old_qbc.arr[i_old]), &(new_qbc.arr[i_new])) == 0)
                qbr_tmp.action = NO_ACTION;
            else
                qbr_tmp.action = ACTION_UPDATE;
            i_old++;
            i_new++;
        } else {// old's name > new's name
            qbr_tmp = new_qbc.arr[i_new];
            qbr_tmp.action = ACTION_ADD;
            i_new++;
        }

        qbc_append(&res_qbc, qbr_tmp);

    }
//    printf("i_new %d new_qbc.i %d i_old %d , old_qbc.i, %d\n", i_new, new_qbc.i, i_old, old_qbc.i);
    if (i_new >= new_qbc.i) {
        while (i_old < old_qbc.i) {
            qbr_tmp = old_qbc.arr[i_old++];
            time(&qbr_tmp.last_modification.tv_sec);
            qbr_tmp.action = ACTION_DELETE;
            qbc_append(&res_qbc, qbr_tmp);
        }
    } else if (i_old >= old_qbc.i) {
        while (i_new < new_qbc.i) {
            qbr_tmp = new_qbc.arr[i_new++];
            qbr_tmp.action = ACTION_ADD;
            qbc_append(&res_qbc, qbr_tmp);
        }
    }
    return res_qbc;
}

/**
 *
 * @param qbc_server - @c qbox_container_t loaded from file on server side
 * @param qbc_client - qbox_container_t loaded from @c qbc_diff_client on client side
 * @result qbc_server_new  - new qbc that should be saved to SERVER_DB_NAME
 * @result qbc_for_client - qbc that will be sent to client and  used for file exchange as a to-do for client and server
 * @result qbc_move_to_archive - qbc of files that will be moved to archive
 */
void qbc_server_diff(qbox_container_t qbc_server, qbox_container_t qbc_client,
                     qbox_container_t *qbc_server_new, qbox_container_t *qbc_for_client,
                     qbox_container_t *qbc_move_to_archive) {
    //Results:
    //a) get from client qbc
    //b) send _to_client_qbc
    //c) new Server list.
    int i_serv = 0, i_cli = 0;

    *qbc_server_new = qbc_empty(100);
    *qbc_move_to_archive = qbc_empty(100);

    *qbc_for_client = qbc_empty(100);
    //ACTION_REQUEST - client has to send  it to server
    //ACION_DELETE - client has to delete it
    //ACTION_ADD - client has to download it



    int comp_name, comp_time;

    qbox_db_record_t *serv_qbr = NULL, *cli_qbr = NULL;

    while (i_serv < qbc_server.i && i_cli < qbc_client.i) {
        serv_qbr = &qbc_server.arr[i_serv];
        cli_qbr = &qbc_client.arr[i_cli];


        comp_name = comp_qbr_names(serv_qbr, cli_qbr);
        //<new -- delete old
        if (comp_name < 0) {//serv<client
            //server has, client doesnt

            qbc_append(qbc_server_new, *serv_qbr);// To preserve history


            if (serv_qbr->action != ACTION_DELETE) {
                qbc_append_with_action(qbc_for_client, *serv_qbr, ACTION_ADD);
            }
            i_serv++;

        } else if (comp_name == 0) {// serv  name = client name
            // - both have file
            comp_time = comp_qbr_time(serv_qbr, cli_qbr);
            if (comp_time < 0) {
//                serv_time< client_time

                //Add to server_new
                if (cli_qbr->action == ACTION_DELETE)
                    qbc_append_with_action(qbc_server_new, *cli_qbr, ACTION_DELETE);
                else
                    qbc_append_with_action(qbc_server_new, *cli_qbr, NO_ACTION);

                //Adding to to_archive and for_client qbcs
                if (cli_qbr->action == ACTION_DELETE) {
                    if (serv_qbr->file_type != FILETYPE_DIR)
                        qbc_append_with_action(qbc_move_to_archive, *cli_qbr, NO_ACTION);
                } else if (cli_qbr->action == ACTION_UPDATE) {
                    if (serv_qbr->file_type != FILETYPE_DIR)
                        qbc_append_with_action(qbc_move_to_archive, *serv_qbr, NO_ACTION);
                    if (cli_qbr->file_type != FILETYPE_DIR)
                        qbc_append_with_action(qbc_for_client, *cli_qbr, ACTION_REQUEST);
                } else if (cli_qbr->action == ACTION_ADD)
                    fprintf(stderr, "ACTION_ADD shouldnt be in qbc if there is that file on server\n");


            } else if (comp_time == 0) {


                if (serv_qbr->action == ACTION_DELETE && cli_qbr->action == ACTION_DELETE)
                    fprintf(stderr, "Two delete records met while checking client and server things. bad\n");

                if (serv_qbr->file_type != cli_qbr->file_type)
                    fprintf(stderr, "Two the same qbr with different types at server. That's bad\n");

                qbc_append(qbc_server_new, *serv_qbr);
            } else {
                //servertime > client time
                qbc_append(qbc_server_new, *serv_qbr);

                if (serv_qbr->action == ACTION_DELETE)
                    qbc_append(qbc_for_client, *serv_qbr);
                else {
                    serv_qbr->action = ACTION_UPDATE;
                    qbc_append(qbc_for_client, *serv_qbr);
                }
            }


            i_cli++;
            i_serv++;
        } else {
//            serv file name > client file name
            //client has, server doest
            qbc_append_with_action(qbc_server_new, *cli_qbr, NO_ACTION);
            if (cli_qbr->action != ACTION_ADD)
                fprintf(stderr, "Client doesnt add thing, but there is no such thing on server  %s\n",
                        cli_qbr->filename);

            if (cli_qbr->file_type != FILETYPE_DIR)
                qbc_append_with_action(qbc_for_client, *cli_qbr, ACTION_REQUEST);

            i_cli++;
        }

    }

    qbox_db_record_t qbr_tmp;

    if (i_cli >= qbc_client.i) { // server files left
        while (i_serv < qbc_server.i) {
            qbr_tmp = qbc_server.arr[i_serv++];
            qbc_append(qbc_server_new, qbr_tmp);

            if (qbr_tmp.action != ACTION_DELETE) {
                qbr_tmp.action = ACTION_ADD;
                qbc_append(qbc_for_client, qbr_tmp);
            }
        }
    } else if (i_serv >= qbc_server.i) { // client_files_left
        while (i_cli < qbc_client.i) {
            qbr_tmp = qbc_client.arr[i_cli++];
            if (qbr_tmp.action == ACTION_ADD) {
                if (qbr_tmp.file_type != FILETYPE_DIR)
                    qbc_append_with_action(qbc_for_client, qbr_tmp, ACTION_REQUEST);
                qbc_append_with_action(qbc_server_new, qbr_tmp, NO_ACTION);
            } else if (qbr_tmp.action == ACTION_DELETE) {// TODO - is that needed?
                qbc_append(qbc_move_to_archive, qbr_tmp);
                qbc_append(qbc_server_new, qbr_tmp);

            }
        }
    }


}


