//
// Created by jacko on 12.05.17.
//

#ifndef QBOX_COMMON_H
#define _XOPEN_SOURCE 700

#include <time.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <memory.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <libgen.h>
#include <netdb.h>
#include <ftw.h>
#include <stdio.h>
#include "structures.h"
#define BUF_BIG 1024
#define BUF_MED 512
#define BUF_SMALL 256
#define  DEFAULT_MODE 0770
#define PENDING_CONNECTION_LIMIT 8
#define MAX_SYNC_INTERVAL 2
#define SERVER_DB_NAME ".qbox_server_db"
#define CLIENT_DB_NAME ".qbox_client_db"
#define ARCHIVE_DIR_NAME ".arch"
#define PACKET_SIZE 2048



void printAndQuit(const char *str, ...);

/**
 *
 * @param filename - name of file you wonder whether you should ignore while using qbc_from_tree
 * @return  1 if name should be ignored in creating database file in qbox client,  0 otherwise.
 *@see qbc_from_tree
 */
int name_ignored(char *filename);


void qbc_append_with_action(qbox_container_t *qbc, qbox_db_record_t qbr, short action_override);

qbox_container_t qbc_empty(size_t size);

void qbc_append(qbox_container_t *qbc, qbox_db_record_t qbr);

qbox_db_record_t qbr_create(short action, file_type_t type, char *fname, time_t sec, long n_sec);

void sort_qbc_by_name(qbox_container_t *qbc);

int comp_qbr_names(qbox_db_record_t const *qbr1, qbox_db_record_t const *qbr2);

int comp_qbr_time(qbox_db_record_t const *qbr1, qbox_db_record_t const *qbr2);

qbox_container_t qbc_form_tree();

void qbc_print(qbox_container_t qbc);

//struct qbox_container_t qbc_load_form_file( const char * filename);
int qbc_load_form_file(qbox_container_t *qbc, const char *filename);

int qbc_save_to_file(qbox_container_t qbc, const char *filename);

qbox_container_t qbc_diff_client(qbox_container_t new_qbc, qbox_container_t old_qbc);

void qbr_print(qbox_db_record_t qbr);

void qbc_pretty_print(qbox_container_t qbc, int print_action);

void qbr_pretty_print(qbox_db_record_t qbr, int print_action);

int qbc_save_to_file(qbox_container_t qbc, const char *filename);

void qbc_server_diff(qbox_container_t qbc_server, qbox_container_t qbc_client,
                     qbox_container_t *qbc_server_new, qbox_container_t *qbc_for_client,
                     qbox_container_t *qbc_move_to_archive);

void htobe_timespec_in_place(struct timespec *ts);

#define QBOX_COMMON_H


#endif //QBOX_COMMON_H
