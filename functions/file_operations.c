//
// Created by jacko on 13.05.17.
//

#include <string.h>
#include <ftw.h>
#include <libgen.h>
#include "file_operations.h"
#include "socket_functions.h"
#include "structures.h"

int save_file_from_socket(int socketfd) {

    file_info_message_t fileInfo;

    if (read(socketfd, &fileInfo, sizeof(fileInfo)) != sizeof(fileInfo))
        fprintf(stderr, "Cannot read file info");

    betoh_file_info_message_in_place(&fileInfo);

    printf("Received fileinfo %s , %li", fileInfo.filename, fileInfo.file_size);
    fflush(stdout);

    recursive_mkdir(fileInfo.filename, DEFAULT_MODE);

    int output_fd = open(fileInfo.filename, O_EXCL | O_CREAT | O_WRONLY, fileInfo.mode);

    char buf[fileInfo.file_size];
    // whole file not divided TODO - change that

    size_t  bytes_to_read=fileInfo.file_size;
    ssize_t bytes_in_this_read=0;
    size_t bytes_already_read=0;
    while (bytes_to_read>0){

        bytes_in_this_read= read(socketfd, buf+bytes_already_read, bytes_to_read);
        bytes_to_read-=bytes_in_this_read;
        bytes_already_read=fileInfo.file_size-bytes_to_read;
        printf("Reading file. Already : %li, in this read: %li  left: %li\n",bytes_already_read,bytes_in_this_read,bytes_to_read);
    }


    printf("Bytes read from socket %li\n", bytes_already_read+bytes_in_this_read);
    ssize_t bytes_written = write(output_fd, buf, fileInfo.file_size);
    printf("Bytes written to file %li\n", bytes_written);
    close(output_fd);

    return 0;


}


int send_file_to_socket(char *filename, int socketfd) {


    int input_fd = open(filename, O_RDONLY);
    if (input_fd < 0)
        printAndQuit("Cannot open file to send\n");

    struct stat stat_buf;
    if (fstat(input_fd, &stat_buf) < 0)
        printAndQuit("Cannot  fstat\n");


    file_info_message_t fileInfo;
    strcpy(fileInfo.filename, filename);
    size_t size = fileInfo.file_size = (size_t) stat_buf.st_size;
    fileInfo.last_access = stat_buf.st_atim;
    fileInfo.last_modification = stat_buf.st_mtim;
    fileInfo.mode = stat_buf.st_mode;
    fileInfo.msg_type = FILE_INFO;

    printf("Sending %s of size %ld\n",filename,size);

    htobe_file_info_message_in_place(&fileInfo);

    write(socketfd, &fileInfo, sizeof(fileInfo));

    //todo - cant i do it other way without buf?
    char buf[size];

    if (read(input_fd, buf, size) < 0)
        printAndQuit("Cannot read %s \n", size);
    if (write(socketfd, buf, size) < 0) {
        printAndQuit("Cannot write to socket\n");
    }
    close(input_fd);
    return 0;
}


void move_to_archive(char const *relative_path, const char *archive_dir) {
    //relative path : ./dir/file

    struct stat stats;
    lstat(relative_path, &stats);
    char *new_path = malloc(1024);
    strcpy(new_path, archive_dir);
    strcat(new_path, relative_path + 1);// get rid of dot
    //Todo - change mode
    recursive_mkdir(new_path, 0777);

    char buf[30];
    sprintf(buf, ".%li.%li", stats.st_mtim.tv_sec, stats.st_mtim.tv_nsec);
    strcat(new_path, buf);
    rename(relative_path + 2, new_path);
    printf("%s -> %s", relative_path + 2, new_path);
    free(new_path);


}


int recursive_mkdir(char const *path, mode_t mode) {
    char original_path[255];
    getcwd(original_path, 255);

    char *tmp = malloc(FILENAME_LEN * sizeof(char));
    strcpy(tmp, path);

    char *pos, *prev_pos = tmp;
    for (;;) {
        if (prev_pos == NULL)break;
        pos = strchr(prev_pos, '/');
        if (pos == NULL)//last - file maybe
            break;
        *pos = '\0';

        mkdir(prev_pos, mode);

        if (chdir(prev_pos) < 0) {
            fprintf(stderr, "Chdir error for %s \n", prev_pos);
            return -1;
        }


        prev_pos = pos + 1;
    }

    free(tmp);
    if (chdir(original_path) < 0) {
        fprintf(stderr, "Chdir error for %s \n", original_path);
        return -1;
    }
    return 0;

}


int get_qbr_from_archive_fname(qbox_db_record_t *qbr, const char *fname) {
//    strrchr() -- last index of char
    char fname_cp[256];
    strcpy(fname_cp, fname);
    qbox_db_record_t res;
    char *tmp = strrchr(fname_cp, '.');
    if (tmp == NULL)
        return -1;
    res.last_modification.tv_nsec = atoi(tmp + 1);
    *tmp = '\0';
    tmp = strrchr(fname_cp, '.');
    if (tmp == NULL)
        return -1;
    res.last_modification.tv_sec = atoi(tmp + 1);
    *tmp = '\0';
    strcpy(res.filename, fname_cp);

    res.action = NO_ACTION;
    *qbr = res;
    return 0;
}

int qbr_to_archived_filename(qbox_db_record_t qbr, char **res) {
    *res = malloc(BUF_SMALL);
    if (*res == NULL) {
        return -1;
    }
    sprintf(*res, "%s.%li.%li", qbr.filename, qbr.last_modification.tv_sec, qbr.last_modification.tv_nsec);
    return 0;
}

int qbc_from_archived_versions(qbox_container_t *qbc, const char *fname_with_path) {
//    if(qbc->i=
    *qbc = qbc_empty(30);

    if (chdir(ARCHIVE_DIR_NAME) < 0) {
        fprintf(stderr, "Cannot enter archive dir\n");
        return -1;
    }
    char tmp[512];
    strcpy(tmp, fname_with_path);
    char *path = NULL, *fname = NULL;
    fname = strrchr(tmp, '/');
    if (fname == NULL) {
        fprintf(stderr, "Wrong filename - not a path");
        return -1;
    }

    *fname = '\0';
    fname += 1;
    path = tmp;


    int add_files_to_qbc(const char *full_path, const struct stat *stats, int type) {
        char *filename;
        filename = basename((char *) full_path);
        if (name_ignored(filename) || strncmp(filename, fname, strlen(fname)))
            return 0;


        qbox_db_record_t qbr;
        get_qbr_from_archive_fname(&qbr, full_path);
        qbc_append(qbc, qbr);

        return 0;
    }


    ftw(path, add_files_to_qbc, 32);


    chdir("..");
    return 0;
}


