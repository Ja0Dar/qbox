//
// Created by jacko on 13.05.17.
//

#ifndef QBOX_FILE_OPERATIONS_H
//#define _XOPEN_SOURCE 700
#include <ftw.h>
#include <sys/stat.h>
#include "common.h"

int qbc_from_archived_versions(qbox_container_t *qbc, const char *fname_with_path) ;
int qbr_to_archived_filename(qbox_db_record_t qbr, char ** res);
int save_file_from_socket(int socketfd);
int send_file_to_socket(char *filename,int socketfd);
int get_qbr_from_archive_fname(qbox_db_record_t *qbr,const char* fname);

int recursive_mkdir(char const *path, mode_t mode);

void move_to_archive(char const *relative_path,const char *archive_dir) ;

#define QBOX_FILE_OPERATIONS_H

#endif //QBOX_FILE_OPERATIONS_H
