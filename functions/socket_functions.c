
#include <stdio.h>
#include <stdlib.h>
#include "socket_functions.h"
#include "structures.h"

/// with endian support
int send_qbc_to_socket(qbox_container_t qbc, int socketfd) {

    int qbc_i_original = qbc.i;

    qbc = htobe_qbc(qbc);// copies and dynamically alocates... and of cource changes endians

    write(socketfd, &qbc.i, sizeof(qbc.i));
    if (qbc_i_original == 0)return 0;

    if (write(socketfd, qbc.arr, sizeof(qbox_db_record_t) * qbc_i_original) <= 0) {
        perror("Cannot write to socket\n");
        exit(1);
    }
    free(qbc.arr);
    return 0;
}


/// with endian support
int save_qbc_from_socket(qbox_container_t *qbc, int socketfd) {
    //send length
    qbox_container_t tmp_qbc;
    read(socketfd, &tmp_qbc.i, sizeof(tmp_qbc.i));

    int qbc_i_converted= be32toh(tmp_qbc.i);

    if (qbc_i_converted == 0) {
        qbc->maxI = 0;
        qbc->i=0;
        return 0;
    }

    tmp_qbc.arr = calloc((size_t) qbc_i_converted, sizeof(qbox_db_record_t));
    tmp_qbc.maxI = tmp_qbc.i;


    if (read(socketfd, tmp_qbc.arr, sizeof(qbox_db_record_t) *qbc_i_converted) <= 0) {
        perror("Cannot read from socket\n");
        exit(1);
    }
    *qbc=betoh_qbc(tmp_qbc);
    free(tmp_qbc.arr);

    return 0;
}

void setup_serv_addr(struct sockaddr_in *serv_addr, struct hostent *serv_hostent, int portno) {

    bzero((char *) serv_addr, sizeof(struct sockaddr_in));
    serv_addr->sin_family = AF_INET;
    bcopy(serv_hostent->h_addr, (char *) &serv_addr->sin_addr.s_addr,
          serv_hostent->h_length);
    serv_addr->sin_port = htons(portno);

}


int send_int_through_socket(int msg, int socketfd) {
    msg = htobe32(msg);//endians <3
    if (write(socketfd, &msg, sizeof(msg)) < sizeof(msg)) {
        fprintf(stderr, "Cannot send con type to server\n");
        return -1;
    }
    return 0;
}


int save_int_from_socket(int *msg, int socketfd) {
    if (read(socketfd, msg, sizeof(*msg)) < sizeof(*msg)) {
        fprintf(stderr, "Cannot get con type from client\n");
        return -1;
    }

    *msg = be32toh(*msg);// endians
    return 0;
}

void htobe_timespec_in_place(struct timespec *ts) {
    ts->tv_nsec = (long) htobe64((unsigned long) ts->tv_nsec);
    ts->tv_sec = (long) htobe64((unsigned long) ts->tv_sec);
}

void betoh_timespec_in_place(struct timespec *ts) {
    ts->tv_sec = (long) be64toh((unsigned long) ts->tv_sec);
    ts->tv_nsec = be64toh(ts->tv_nsec);
}

qbox_db_record_t htobe_qbr(qbox_db_record_t qbr) {
    htobe_timespec_in_place(&(qbr.last_modification));
    return qbr;
}

qbox_db_record_t betoh_qbr(qbox_db_record_t qbr) {
    betoh_timespec_in_place(&(qbr.last_modification));
    return qbr;
}
/**
 * @param qbc
 * @return qbox_container_t in big endian with dynamically allocated arr
 */
qbox_container_t htobe_qbc(qbox_container_t qbc) {
    qbox_container_t res;
    res.arr=calloc(qbc.i,sizeof(qbox_db_record_t));

    for (int i = 0; i < qbc.i; i++)
        res.arr[i]=htobe_qbr(qbc.arr[i]);


    res.i = htobe32(qbc.i);
    res.maxI= htobe32(qbc.i);// trim it down :)
    return res;
}
/**
 *
 * @param qbc
 * @return qbox_container_t in host endian with dynamically allocated arr
 */
qbox_container_t betoh_qbc(qbox_container_t qbc) {
    qbox_container_t res;
    res.i = be32toh(qbc.i);
    res.maxI = be32toh(qbc.i);//trim
    res.arr=calloc(res.i,sizeof(qbox_db_record_t));
    for (int i = 0; i < res.i; i++) {
        res.arr[i]=betoh_qbr(qbc.arr[i]);
    }
    return res;
}

void betoh_int_in_place(int *x) {
    *x = (int) be32toh((unsigned int) *x);
};


void betoh_file_info_message_in_place(file_info_message_t *fim) {
    fim->file_size = be64toh(fim->file_size);
    betoh_timespec_in_place(&fim->last_modification);
    betoh_timespec_in_place(&fim->last_access);
    fim->mode = be32toh(fim->mode);
};

void htobe_file_info_message_in_place(file_info_message_t *fim) {
    fim->file_size = htobe64(fim->file_size);
    htobe_timespec_in_place(&fim->last_modification);
    htobe_timespec_in_place(&fim->last_access);
    fim->mode = htobe32(fim->mode);
};
