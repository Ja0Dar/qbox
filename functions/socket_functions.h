//
// Created by jacko on 14.05.17.
//

#ifndef QBOX_SOCKET_FUNCTIONS_H
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include "structures.h"
#define _XOPEN_SOURCE 700


enum connection_type {CON_CLI=8,CON_HR};
int save_int_from_socket(int *msg, int socketfd);
int send_int_through_socket(int msg, int socketfd);
void setup_serv_addr(struct sockaddr_in *serv_addr, struct hostent *serv_hostent, int portno);

int save_qbc_from_socket(qbox_container_t *tmp_qbc, int socketfd);
int send_qbc_to_socket(qbox_container_t qbc, int socketfd);





qbox_container_t htobe_qbc(qbox_container_t qbc);
qbox_container_t betoh_qbc(qbox_container_t qbc);

qbox_db_record_t htobe_qbr(qbox_db_record_t qbr);
qbox_db_record_t betoh_qbr(qbox_db_record_t qbr);

void htobe_timespec_in_place(struct timespec *ts);
void betoh_timespec_in_place(struct timespec *ts);

void htobe_file_info_message_in_place(file_info_message_t *fim);
void betoh_file_info_message_in_place(file_info_message_t * fim);

void betoh_int_in_place(int *x);
#define QBOX_SOCKET_FUNCTIONS_H

#endif //QBOX_SOCKET_FUNCTIONS_H
