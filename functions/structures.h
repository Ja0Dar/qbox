//
// Created by jacko on 06.06.17.
//

#include <sys/stat.h>
#include <time.h>

#ifndef QBOX_STRUCTURES_H
//Not needed probably
#define FILENAME_LEN  255
//todo - remove?
typedef enum __attribute__ ((packed)) {
    FILE_INFO
} msgtype;

//Force enum to be one byte (short) :
//todo - check if works
typedef enum __attribute__ ((packed)) {
    ACTION_DELETE = 3, ACTION_UPDATE, ACTION_ADD, NO_ACTION, ACTION_REQUEST
} actions;
enum __attribute__ ((packed)) {
    FILETYPE_REGULAR = 10, FILETYPE_DIR
} typedef file_type_t;


struct {
    short msg_type;
    size_t file_size;
    char filename[FILENAME_LEN];
    struct timespec last_modification;
    struct timespec last_access;
    mode_t mode;
} typedef file_info_message_t;

// todo - rm
/*
struct packet {
    size_t len;
    char bytes[PACKET_SIZE - sizeof(size_t)];
};
*/

/**
 * \brief record containing informations about file useful for qbox.
 *
 *
 *
 * @code
struct qbox_db_record_t{
    short action;
    file_type_t file_type;
    char filename[FILENAME_LEN];
    struct timespec last_modification;
}typedef qbox_db_record_t;
@endcode
 *
 *@see
 * qbr_create,  qbr_pretty_print qbr_print,
 */
struct qbox_db_record_t {
    short action;
    file_type_t file_type;
    char filename[FILENAME_LEN];
    struct timespec last_modification;


} typedef qbox_db_record_t;


struct qbox_container_t {
    qbox_db_record_t *arr;
    unsigned int i;
    int maxI;
} typedef qbox_container_t;

#define QBOX_STRUCTURES_H

#endif //QBOX_STRUCTURES_H
