#include <time.h>
#include "functions/common.h"
#include "functions/file_operations.h"
#include "functions/socket_functions.h"


int prompt_for_int(int min_included, int max_excluded, const char *prompt);

void parse_hr_args(int argc, char **argv, struct hostent **serv_hostent, int *port_nr);
int main(int argc, char **argv) {
    int sockfd, portno;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    parse_hr_args(argc, argv, &server, &portno);

    //setup socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        printAndQuit("ERROR opening socket");
    }
    setup_serv_addr(&serv_addr, server, portno);

    if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        printAndQuit("Cannot connect to server\n");
    }

    send_int_through_socket(CON_HR, sockfd);

    qbox_container_t server_history;
    save_qbc_from_socket(&server_history, sockfd);

    qbc_pretty_print(server_history, 1);

    int chosen_qbr_id = prompt_for_int(0, server_history.i, "File to bring back (int)(-1 to exit)");

    //Send request for file to server
    send_int_through_socket(chosen_qbr_id, sockfd);
    if (chosen_qbr_id == -1) {
        shutdown(sockfd, SHUT_RDWR);
        close(sockfd);
        printf("Cancelled\n");
        exit(0);

    }


    if (server_history.arr[chosen_qbr_id].file_type == FILETYPE_DIR)
        printf("Directory should be  back\n");
    else {

        qbox_container_t file_versions;

        save_qbc_from_socket(&file_versions, sockfd);
        printf("File versions\n");
        qbc_pretty_print(file_versions, 1);

        int version_to_bring_back = prompt_for_int(0, file_versions.i, "Version to bring back (type -1 to exit)");

        send_int_through_socket(version_to_bring_back, sockfd);
        if (version_to_bring_back == -1)
            printf("Cancelled\n");

    }
    shutdown(sockfd, SHUT_RDWR);
    close(sockfd);


}


int prompt_for_int(int min_included, int max_excluded, const char *prompt) {


    int res = min_included - 1;
    while (res < min_included || max_excluded <= res) {

        char *buf = malloc(BUF_SMALL);
        size_t bufsize = BUF_SMALL;

        do {
            printf("%s\n", prompt);
            getline(&buf, &bufsize, stdin);
        } while (sscanf(buf, "%d\n", &res) != 1);
//            printAndQuit("Client decided to not supply int\n");
        free(buf);
        if (res == -1)
            return res;

        if (res < min_included || max_excluded <= res)
            printf("Int should be  %d<= (int) < %d\n", min_included, max_excluded);
    }

    return res;

}

void parse_hr_args(int argc, char **argv, struct hostent **serv_hostent, int *port_nr) {
    if (argc < 3) {
        fprintf(stderr, "usage %s hostname port\n", argv[0]);
        exit(0);
    }
    *serv_hostent = gethostbyname(argv[1]);
    if (*serv_hostent == NULL) {
        fprintf(stderr, "ERROR, no such host\n");
        exit(0);
    }
    *port_nr = atoi(argv[2]);
}

