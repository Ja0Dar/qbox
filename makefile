CC=gcc
FLAGS= -Wall -std=gnu99 -pedantic
DEP= functions/common.c functions/socket_functions.c functions/file_operations.c
all: client server hr 

client: client.c
	$(CC) $(FLAGS) -o $@.out  $^ $(DEP)

server: server.c
	$(CC) $(FLAGS) -o $@.out  $^ $(DEP)

hr: history_retriever.c
	$(CC) $(FLAGS) -o $@.out  $^ $(DEP)

clean:
	rm -f *.out *.o *.so
