#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <time.h>
#include <signal.h>
#include "functions/common.h"
#include "functions/file_operations.h"
#include "functions/socket_functions.h"

int get_portno_form_args(int argc, char **argv);

int create_and_bind_socket_for_server(uint16_t portno);

void exchange_files_with_client(qbox_container_t qbc, int socketfd);

void interact_with_client(int newsockfd);

void interact_with_history_retriever(int newsockfd);

void cleanup_socket();

int socketfd = -1;

int main(int argc, char *argv[]) {
    int newsockfd, portno;
    socklen_t clilen;
    struct sockaddr_in cli_addr;


    portno = get_portno_form_args(argc, argv);

    socketfd = create_and_bind_socket_for_server(portno);
    if (socketfd<0){
        perror("Cannot open socket");
        exit(1);
    }
    atexit(cleanup_socket);
    signal(SIGINT, exit);
    signal(SIGTERM, exit);
    //Start listening on ports
    listen(socketfd, PENDING_CONNECTION_LIMIT);

    int conn_type;
    clilen = sizeof(cli_addr);
    int forever = 1;
    while (forever) {

        newsockfd = accept(socketfd,
                           (struct sockaddr *) &cli_addr,
                           &clilen);
        if (newsockfd < 0)
            fprintf(stderr, "Cannot accept connection\n");
        else {
            if (clilen != sizeof(cli_addr))
                printf("Client len has changed\n");


            if (save_int_from_socket(&conn_type, newsockfd) < 0)printAndQuit("Cannot get client conn type");


            if (conn_type == CON_CLI)
                interact_with_client(newsockfd);
            else if (conn_type == CON_HR)
                interact_with_history_retriever(newsockfd);
            else
                printAndQuit("Not supported conn type\n");


            shutdown(newsockfd, SHUT_RDWR);
            close(newsockfd);
        }
    }
    return 0;
}

void interact_with_history_retriever(int newsockfd) {
    qbox_container_t serv_qbc;

    qbc_load_form_file(&serv_qbc, SERVER_DB_NAME);
    send_qbc_to_socket(serv_qbc, newsockfd);

    int requested_file_id;
    save_int_from_socket(&requested_file_id, newsockfd);

    if (requested_file_id == -1) {
        shutdown(newsockfd, SHUT_RDWR);
        close(newsockfd);
        return;
    }

    if (serv_qbc.arr[requested_file_id].file_type == FILETYPE_DIR) {
        serv_qbc.arr[requested_file_id].action = NO_ACTION;//TODO avoid too much writting by atexits
    } else {


        qbox_container_t versions_of_file;
        qbc_from_archived_versions(&versions_of_file, serv_qbc.arr[requested_file_id].filename);

        send_qbc_to_socket(versions_of_file, newsockfd);

        int version_id;
        save_int_from_socket(&version_id, newsockfd);

        if (version_id == -1) {
            shutdown(newsockfd, SHUT_RDWR);
            close(newsockfd);
            return;
        }

        //todo - bring back file
//            if file was deleted - update Action,time, bring back
        //if    file wasnt deleted - move file to archive, update time, bring back
        if (serv_qbc.arr[requested_file_id].action != ACTION_DELETE) {
            move_to_archive(serv_qbc.arr[requested_file_id].filename, ARCHIVE_DIR_NAME);
        }


        char old_path[BUF_MED];
        strcpy(old_path, "./"ARCHIVE_DIR_NAME);

        char *buf = NULL;
        qbr_to_archived_filename(versions_of_file.arr[version_id], &buf);
        strcat(old_path, buf + 1);
        free(buf);
        char *new_path = serv_qbc.arr[requested_file_id].filename + 2;
        printf("\nold : %s \nnew : %s\n", old_path + 2, new_path);

        recursive_mkdir(new_path, DEFAULT_MODE);
        if (rename(old_path + 2, new_path)) {
            printAndQuit("Cannot move file when bringing it back \n");
        }

        serv_qbc.arr[requested_file_id].action = NO_ACTION;
        time(&serv_qbc.arr[requested_file_id].last_modification.tv_sec);



        //save serv_qbc to file
        free(versions_of_file.arr);
    }
    qbc_save_to_file(serv_qbc, SERVER_DB_NAME);
    free(serv_qbc.arr);

    //todo history retriever stuff

}

void interact_with_client(int newsockfd) {
    qbox_container_t serv_qbc;

    qbc_load_form_file(&serv_qbc, SERVER_DB_NAME);

    qbox_container_t client_qbc;
    save_qbc_from_socket(&client_qbc, newsockfd);
    printf("Received client qbc form socket\n");
    qbc_print(client_qbc);

    qbox_container_t serv_qbc_new, to_archive_qbc, for_client_qbc;

    qbc_server_diff(serv_qbc, client_qbc, &serv_qbc_new, &for_client_qbc, &to_archive_qbc);


    printf("To delete\n");
    qbc_pretty_print(to_archive_qbc, 1);
    for (int i = 0; i < to_archive_qbc.i; i++)
        move_to_archive(to_archive_qbc.arr[i].filename, ARCHIVE_DIR_NAME);

    printf("for client qbc\n");
    qbc_print(for_client_qbc);


    send_qbc_to_socket(for_client_qbc, newsockfd);

    exchange_files_with_client(for_client_qbc, newsockfd);

    qbc_save_to_file(serv_qbc_new, SERVER_DB_NAME);
}

int create_and_bind_socket_for_server(uint16_t portno) {
    struct sockaddr_in serv_addr;
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);//TODO there might be sigpipe if dead
    if (sockfd < 0) {
        return sockfd;
    }

    bzero((char *) &serv_addr, sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno); // convert to network byte order


    //bind serv_addr to socket
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        return -1;

    return sockfd;
}

int get_portno_form_args(int argc, char **argv) {
    if (argc < 2) {
        fprintf(stderr, "ERROR, no port provided\n");
        exit(1);
    }
    return atoi(argv[1]);
}


void exchange_files_with_client(qbox_container_t qbc, int socketfd) {
    // Action names should be percieved from client side
    qbox_db_record_t *qbr;

    for (int i = 0; i < qbc.i; i++) {
        qbr = &qbc.arr[i];
        switch (qbr->action) {
            case ACTION_ADD:
                if (qbr->file_type != FILETYPE_DIR) {
                    if (send_file_to_socket(qbr->filename, socketfd) < 0)
                        fprintf(stderr, "cannot send file");
                    printf("File sent %s\n", qbr->filename);
                }
                break;
            case ACTION_DELETE:
                //client deletes stuff
                break;
            case ACTION_UPDATE:
                if (qbr->file_type != FILETYPE_DIR) {
                    if (send_file_to_socket(qbr->filename, socketfd) < 0)
                        fprintf(stderr, "cannot send file");
                    else
                        printf("File sent %s\n", qbr->filename);
                }
                break;

            case ACTION_REQUEST:
                if (qbr->file_type == FILETYPE_DIR)
                    fprintf(stderr, "Requesting dir shouldnt be executed\n");
                else {
                    if (save_file_from_socket(socketfd) < 0)
                        printf("Cannot save file \n");
                    else
                        printf("saved file %s\n", qbr->filename);
                }
                break;

            default:
                printAndQuit("WRONG ACTION  while processing server response %d\n", qbr->action);

        }
    }
}

void cleanup_socket() {

    if (shutdown(socketfd, SHUT_RDWR) < 0)
        printf("Couldn't shutdown socket %d", socketfd);
    else
        printf("Socket shutdown\n");
    if (close(socketfd) < 0)
        printf("Couldn't close socket\n");
    else
        printf("Socket closed\n");
}
