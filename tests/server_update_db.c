#define _XOPEN_SOURCE 700

#include <ftw.h>
#include <libgen.h>
#include "../functions/common.h"

int save_tree_to_global_fd(const char *path, const struct stat *stats, int type, struct FTW *ftw);

int save_tree_to_global_qbc_ptr(const char *path, const struct stat *stats, int type, struct FTW *ftw);


void browseTree_nftw(char path[80], int (*func)(const char *, const struct stat, int, struct FTW *));


//void * nftw_ptr=NULL;
//void * nftw_current_ptr=NULL;

int global_fd = 0;

int main() {


    qbox_container_t new_qbc = qbc_form_tree();
//    printf("NEW\n");
//    qbc_print(new_qbc);
    qbox_container_t old_qbc = qbc_load_form_file(CLIENT_DB_NAME);

    printf("\nOLD\n");
    qbc_print(old_qbc);



    qbox_container_t res_qbc = qbc_diff_client(new_qbc, old_qbc);

    struct qbox_db_record_t qbr_tmp;
    for (int i = 0; i < res_qbc.i; i++) {
        qbr_tmp = res_qbc.arr[i];
        if (qbr_tmp.action == ACTION_DELETE) {
            printf("DELET %s\n", qbr_tmp.filename);
        } else if (qbr_tmp.action == ACTION_UPDATE) {
            printf("UPDATE %s\n", qbr_tmp.filename);
        } else if (qbr_tmp.action == ACTION_ADD)
            printf("ADDED %s\n", qbr_tmp.filename);
    }

//    printf("\nRES\n");
//    qbc_print(res_qbc);

    qbox_container_t updated_qbc =  qbc_empty((size_t) res_qbc.maxI);
    for (int i = 0; i < res_qbc.i; i++)
        if (res_qbc.arr[i].action != ACTION_DELETE)
            qbc_append_with_action(&updated_qbc, res_qbc.arr[i],NO_ACTION);


    printf("\nUPDATED\n");
    qbc_print(updated_qbc);
    qbc_save_to_file(updated_qbc, CLIENT_DB_NAME);



    free(old_qbc.arr);
    free(new_qbc.arr);
    free(updated_qbc.arr);
    free(res_qbc.arr);
}


void browseTree_nftw(char path[80], int (*func)(const char *, const struct stat, int, struct FTW *)) {
    nftw(path, save_tree_to_global_fd, 32, FTW_CHDIR);

}


int save_tree_to_global_fd(const char *path, const struct stat *stats, int type, struct FTW *ftw) {
    char *filename;
    filename = basename((char *) path);
    if (name_ignored(filename))
        return 0;

    char buf[FILENAME_LEN + 5 + 2 * sizeof(long int)];

    qbox_db_record_t qbr;
    strcpy(qbr.filename, path);
    qbr.action = 0;//todo d change
    qbr.last_modification = stats->st_mtim;

    write(global_fd, &qbr, sizeof(qbr));

    return 0;
}

