#include <sys/time.h>
#include <time.h>
#include "../functions/common.h"
#include <limits.h>
#include "../functions/file_operations.h"
#include <sys/inotify.h>


#define NOTIFY_BUF_LEN (10 * (sizeof(struct inotify_event) + NAME_MAX+ 1))

static void             /* Display information from inotify_event structure */
displayInotifyEvent(struct inotify_event *i) {
    printf("    wd =%2d; ", i->wd);
    if (i->cookie > 0)
        printf("cookie =%4d; ", i->cookie);

    printf("mask = ");
    if (i->mask & IN_ACCESS) printf("IN_ACCESS ");
    if (i->mask & IN_ATTRIB) printf("IN_ATTRIB ");
    if (i->mask & IN_CLOSE_NOWRITE) printf("IN_CLOSE_NOWRITE ");
//    if (i->mask & IN_CLOSE_WRITE) printf("IN_CLOSE_WRITE ");
    if (i->mask & IN_CREATE) printf("IN_CREATE ");
    if (i->mask & IN_DELETE) printf("IN_DELETE ");
    if (i->mask & IN_DELETE_SELF) printf("IN_DELETE_SELF ");
    if (i->mask & IN_IGNORED) printf("IN_IGNORED ");
    if (i->mask & IN_ISDIR) printf("IN_ISDIR ");
    if (i->mask & IN_MODIFY) printf("IN_MODIFY ");
    if (i->mask & IN_MOVE_SELF) printf("IN_MOVE_SELF ");
    if (i->mask & IN_MOVED_FROM) printf("IN_MOVED_FROM ");
    if (i->mask & IN_MOVED_TO) printf("IN_MOVED_TO ");
    if (i->mask & IN_OPEN) printf("IN_OPEN ");
    if (i->mask & IN_Q_OVERFLOW) printf("IN_Q_OVERFLOW ");
    if (i->mask & IN_UNMOUNT) printf("IN_UNMOUNT ");
    printf("\n");

    if (i->len > 0)
        printf("        name = %s\n", i->name);
}

int main(int argc, char **argv) {

    int inotify_fd, watch;
    struct inotify_event *event;
    inotify_fd = inotify_init();
    if (inotify_fd < 0)
        printAndQuit("CANNOT INIT\n");
    char buf[NOTIFY_BUF_LEN];
    int my_mode=IN_MOVE|IN_MODIFY|IN_CREATE|IN_DELETE|IN_CLOSE_WRITE;
    watch = inotify_add_watch(inotify_fd, argv[1],my_mode);

    ssize_t numRead;
    if (watch < 0)
        printAndQuit("Cannot add watch\n");


    char *p;
    for (;;) {                                  /* Read events forever */
        numRead = read(inotify_fd, buf, NOTIFY_BUF_LEN);
        if (numRead == 0)
            printAndQuit("read() from inotify fd returned 0!");

        if (numRead == -1)
            printAndQuit("read");

        printf("Read %ld bytes from inotify fd\n", (long) numRead);

        /* Process all of the events in buffer returned by read() */

        for (p = buf; p < buf + numRead;) {
            event = (struct inotify_event *) p;
            displayInotifyEvent(event);

            p += sizeof(struct inotify_event) + event->len;
        }
    }

//
//    qbox_container_t qbc = qbc_form_tree();
//    char * str;
//    qbr_to_archived_filename(qbc.arr[0],&str);
//    printf("%s",str);


/*
    //Checking server difff

  qbox_container_t serv_old_qbc, serv_new_qbc, client_qbc, for_client_qbc, to_archive_qbc;
    serv_old_qbc=setup_serv_old();
    client_qbc=setup_client();

    qbc_server_diff(serv_old_qbc,client_qbc,&serv_new_qbc,&for_client_qbc,&to_archive_qbc);



    printf("\nSERV_NEW\n"); qbc_print(serv_new_qbc);
    printf("\nTO_ARCHIVE\n"); qbc_print(to_archive_qbc);
    printf("\nFOR_CLIENT\n"); qbc_print(for_client_qbc);
//*/


//    recursive_mkdir("a/b/c/d/e/",0777);
//    recursive_mkdir("b/b/c/d/e/f",0777);
//    move_to_archive("./dir/file",".arch");
}

/*
qbox_container_t setup_serv_old() {
    qbox_container_t qbc = qbc_empty(100);
    qbox_db_record_t qbr;
    qbr = qbr_create(NO_ACTION, "a", 10,10);
    qbc_append(&qbc, qbr);

    qbr = qbr_create(NO_ACTION, "b", 10,10);
    qbc_append(&qbc, qbr);

    qbr = qbr_create(ACTION_DELETE, "c", 12,10);
    qbc_append(&qbc, qbr);

    qbr = qbr_create(NO_ACTION, "d", 12,10);
    qbc_append(&qbc, qbr);


    qbr = qbr_create(NO_ACTION, "e", 10,10);
    qbc_append(&qbc, qbr);

    qbr = qbr_create(NO_ACTION, "g", 10,10);
    qbc_append(&qbc, qbr);

    qbr = qbr_create(NO_ACTION, "h", 10,10);
    qbc_append(&qbc, qbr);
    return qbc;
}

qbox_container_t setup_client() {
    qbox_container_t qbc = qbc_empty(100);
    qbox_db_record_t qbr;
    qbr = qbr_create(ACTION_UPDATE, "a", 12,10);
    qbc_append(&qbc, qbr);

//    qbr = qbr_create(NO_ACTION, "b", 10);
//    qbc_append(&qbc, qbr);

    qbr = qbr_create(NO_ACTION, "c", 10,10);
    qbc_append(&qbc, qbr);

    qbr = qbr_create(NO_ACTION, "d", 10,10);
    qbc_append(&qbc, qbr);


    qbr = qbr_create(ACTION_ADD, "f", 10,10);
    qbc_append(&qbc, qbr);

    qbr = qbr_create(ACTION_UPDATE, "g", 12,10);
    qbc_append(&qbc, qbr);

    qbr = qbr_create(ACTION_DELETE, "h", 12,10);
    qbc_append(&qbc, qbr);
    return qbc;
}

*/